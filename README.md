# NekoBot

## Prerequire

- Node.js 8.x

## Development

### Environment variables

```bash
$ cp .env.sample .env
$ vim .env
```

- LINE_CHANNEL_SECRET
- LINE_CHANNEL_ACCESS_TOKEN

### Get started

```bash
$ git clone git@gitlab.com:KinokoTeam/LineSeminar/nekobot.git
$ cd nekobot
$ npm install
$ npm run build
$ npm run start
```

### Run Ngrok

```bash
$ ngrok http 3000
```

## Deployment

```bash
$ npm run release-build
```
