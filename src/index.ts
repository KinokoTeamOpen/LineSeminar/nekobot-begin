require('dotenv').config();
import * as express from 'express';
import * as line from '@line/bot-sdk';
import { WebhookEvent } from '@line/bot-sdk';
import { nekoData, NekoClient, Nekonverter } from './neko';

// 猫の初期設定
const nekoClient = new NekoClient(nekoData);

// LINE BOT の設定
const config = {
  channelSecret: process.env.LINE_CHANNEL_SECRET,
  channelAccessToken: process.env.LINE_CHANNEL_ACCESS_TOKEN
};
const botClient = new line.Client(config);

// Express 初期設定
const app = express();
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'PUT');
  next();
});

app.get('/', (req, res) => {
  const neko = Nekonverter.convertKindToString(nekoClient.getRandom().kind);
  res.send(`Hello NekoBot! Today's No.1 Neko is a '${neko}'`);
});

/**
 * Handling LINE messaging event.
 *
 * @param evt WebhookEvent
 */
async function handleEvent_Step1(evt: WebhookEvent): Promise<any> {
  if (evt.type === 'message') {
    if (evt.source.userId === 'Udeadbeefdeadbeefdeadbeefdeadbeef') {
      return 'WebhookVerifyRequest';
    }
    if (evt.message.type === 'text') {
      return botClient.replyMessage(evt.replyToken, {
        type: 'text',
        text: evt.message.text
      });
    }
  }
  return Promise.resolve(null);
}

app.post('/webhook', line.middleware(config), (req, res) => {
  console.log(req.body.events);
  Promise.all(req.body.events.map(handleEvent_Step1)).then(result => res.json(result));
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`);
});
