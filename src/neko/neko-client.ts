import '../array.extension';
import { INeko } from './neko';
import { NekoType } from './neko-type';

export class NekoClient {
  private readonly nekoArray: INeko[];

  constructor(nekoArray: INeko[]) {
    this.nekoArray = [].concat(nekoArray);
  }

  getRandom(): INeko {
    this.nekoArray.shuffle();
    return this.nekoArray[0];
  }

  getRandomByKind(kind: NekoType): INeko {
    const filteredNekoArray = this.nekoArray.filter(neko => neko.kind);
    filteredNekoArray.shuffle();
    return filteredNekoArray[0];
  }
}
