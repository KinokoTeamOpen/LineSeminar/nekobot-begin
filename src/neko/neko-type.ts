export enum NekoType {
  /**
   * マンチカン
   */
  Munchkin,
  /**
   * スコティッシュフォールド
   */
  ScottishFold,
  /**
   * ラグドール
   */
  Ragdoll,
  /**
   * メインクーン
   */
  MaineCoon,
  /**
   * ロシアンブルー
   */
  RussianBlue,
  /**
   * エキゾチックショートヘア
   */
  ExoticShorthair,
  /**
   * アメリカンショートヘア
   */
  AmericanShorthair,
  /**
   * ノルウェージャンフォレストキャット
   */
  NorwegianForestCat,
  /**
   * ソマリ
   */
  Somali,
  /**
   * ベンガル
   */
  Bengal,
  /**
   * その他
   */
  Other
}
