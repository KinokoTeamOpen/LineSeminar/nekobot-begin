import { NekoType } from './neko-type';
const STORAGE_URL = 'https://storage.googleapis.com/kinoko-line-seminer.appspot.com';

export interface INeko {
  type: 'image' | 'video';
  kind: NekoType;
  src: string;
  thumbnail?: string;
}

export const nekoData: INeko[] = [
  {
    type: 'image',
    kind: NekoType.AmericanShorthair,
    src: `${STORAGE_URL}/AmericanShorthair/1.jpg`
  },
  {
    type: 'image',
    kind: NekoType.AmericanShorthair,
    src: `${STORAGE_URL}/AmericanShorthair/2.jpg`
  },
  {
    type: 'image',
    kind: NekoType.AmericanShorthair,
    src: `${STORAGE_URL}/AmericanShorthair/3.jpg`
  },
  {
    type: 'image',
    kind: NekoType.AmericanShorthair,
    src: `${STORAGE_URL}/AmericanShorthair/4.jpg`
  },
  {
    type: 'image',
    kind: NekoType.AmericanShorthair,
    src: `${STORAGE_URL}/AmericanShorthair/5.jpg`
  },
  {
    type: 'image',
    kind: NekoType.AmericanShorthair,
    src: `${STORAGE_URL}/AmericanShorthair/6.jpg`
  },
  {
    type: 'image',
    kind: NekoType.AmericanShorthair,
    src: `${STORAGE_URL}/AmericanShorthair/7.jpg`
  },
  {
    type: 'image',
    kind: NekoType.AmericanShorthair,
    src: `${STORAGE_URL}/AmericanShorthair/8.jpg`
  },
  {
    type: 'image',
    kind: NekoType.AmericanShorthair,
    src: `${STORAGE_URL}/AmericanShorthair/9.jpg`
  },
  {
    type: 'image',
    kind: NekoType.AmericanShorthair,
    src: `${STORAGE_URL}/AmericanShorthair/10.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Bengal,
    src: `${STORAGE_URL}/Bengal/1.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Bengal,
    src: `${STORAGE_URL}/Bengal/2.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Bengal,
    src: `${STORAGE_URL}/Bengal/3.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Bengal,
    src: `${STORAGE_URL}/Bengal/4.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Bengal,
    src: `${STORAGE_URL}/Bengal/5.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Bengal,
    src: `${STORAGE_URL}/Bengal/6.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Bengal,
    src: `${STORAGE_URL}/Bengal/7.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Bengal,
    src: `${STORAGE_URL}/Bengal/8.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Bengal,
    src: `${STORAGE_URL}/Bengal/9.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Bengal,
    src: `${STORAGE_URL}/Bengal/10.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Bengal,
    src: `${STORAGE_URL}/Bengal/11.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Bengal,
    src: `${STORAGE_URL}/Bengal/12.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Bengal,
    src: `${STORAGE_URL}/Bengal/13.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ExoticShorthair,
    src: `${STORAGE_URL}/ExoticShorthair/1.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ExoticShorthair,
    src: `${STORAGE_URL}/ExoticShorthair/1.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ExoticShorthair,
    src: `${STORAGE_URL}/ExoticShorthair/2.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ExoticShorthair,
    src: `${STORAGE_URL}/ExoticShorthair/3.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ExoticShorthair,
    src: `${STORAGE_URL}/ExoticShorthair/4.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ExoticShorthair,
    src: `${STORAGE_URL}/ExoticShorthair/5.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ExoticShorthair,
    src: `${STORAGE_URL}/ExoticShorthair/6.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ExoticShorthair,
    src: `${STORAGE_URL}/ExoticShorthair/7.jpg`
  },
  {
    type: 'image',
    kind: NekoType.MaineCoon,
    src: `${STORAGE_URL}/MaineCoon/1.jpg`
  },
  {
    type: 'image',
    kind: NekoType.MaineCoon,
    src: `${STORAGE_URL}/MaineCoon/2.jpg`
  },
  {
    type: 'image',
    kind: NekoType.MaineCoon,
    src: `${STORAGE_URL}/MaineCoon/3.jpg`
  },
  {
    type: 'image',
    kind: NekoType.MaineCoon,
    src: `${STORAGE_URL}/MaineCoon/4.jpg`
  },
  {
    type: 'image',
    kind: NekoType.MaineCoon,
    src: `${STORAGE_URL}/MaineCoon/5.jpg`
  },
  {
    type: 'image',
    kind: NekoType.MaineCoon,
    src: `${STORAGE_URL}/MaineCoon/6.jpg`
  },
  {
    type: 'image',
    kind: NekoType.MaineCoon,
    src: `${STORAGE_URL}/MaineCoon/7.jpg`
  },
  {
    type: 'image',
    kind: NekoType.MaineCoon,
    src: `${STORAGE_URL}/MaineCoon/8.jpg`
  },
  {
    type: 'image',
    kind: NekoType.MaineCoon,
    src: `${STORAGE_URL}/MaineCoon/9.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Munchkin,
    src: `${STORAGE_URL}/Munchkin/1.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Munchkin,
    src: `${STORAGE_URL}/Munchkin/2.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Munchkin,
    src: `${STORAGE_URL}/Munchkin/3.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Munchkin,
    src: `${STORAGE_URL}/Munchkin/4.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Munchkin,
    src: `${STORAGE_URL}/Munchkin/5.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Munchkin,
    src: `${STORAGE_URL}/Munchkin/6.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Munchkin,
    src: `${STORAGE_URL}/Munchkin/7.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Munchkin,
    src: `${STORAGE_URL}/Munchkin/8.jpg`
  },
  {
    type: 'image',
    kind: NekoType.NorwegianForestCat,
    src: `${STORAGE_URL}/NorwegianForestCat/1.jpg`
  },
  {
    type: 'image',
    kind: NekoType.NorwegianForestCat,
    src: `${STORAGE_URL}/NorwegianForestCat/2.jpg`
  },
  {
    type: 'image',
    kind: NekoType.NorwegianForestCat,
    src: `${STORAGE_URL}/NorwegianForestCat/3.jpg`
  },
  {
    type: 'image',
    kind: NekoType.NorwegianForestCat,
    src: `${STORAGE_URL}/NorwegianForestCat/4.jpg`
  },
  {
    type: 'image',
    kind: NekoType.NorwegianForestCat,
    src: `${STORAGE_URL}/NorwegianForestCat/5.jpg`
  },
  {
    type: 'image',
    kind: NekoType.NorwegianForestCat,
    src: `${STORAGE_URL}/NorwegianForestCat/6.jpg`
  },
  {
    type: 'image',
    kind: NekoType.NorwegianForestCat,
    src: `${STORAGE_URL}/NorwegianForestCat/7.jpg`
  },
  {
    type: 'image',
    kind: NekoType.NorwegianForestCat,
    src: `${STORAGE_URL}/NorwegianForestCat/8.jpg`
  },
  {
    type: 'image',
    kind: NekoType.NorwegianForestCat,
    src: `${STORAGE_URL}/NorwegianForestCat/9.jpg`
  },
  {
    type: 'image',
    kind: NekoType.NorwegianForestCat,
    src: `${STORAGE_URL}/NorwegianForestCat/10.jpg`
  },
  {
    type: 'image',
    kind: NekoType.NorwegianForestCat,
    src: `${STORAGE_URL}/NorwegianForestCat/11.jpg`
  },
  {
    type: 'image',
    kind: NekoType.NorwegianForestCat,
    src: `${STORAGE_URL}/NorwegianForestCat/12.jpg`
  },
  {
    type: 'image',
    kind: NekoType.NorwegianForestCat,
    src: `${STORAGE_URL}/NorwegianForestCat/13.jpg`
  },
  {
    type: 'image',
    kind: NekoType.NorwegianForestCat,
    src: `${STORAGE_URL}/NorwegianForestCat/14.jpg`
  },
  {
    type: 'image',
    kind: NekoType.NorwegianForestCat,
    src: `${STORAGE_URL}/NorwegianForestCat/15.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Other,
    src: `${STORAGE_URL}/Other/1.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Other,
    src: `${STORAGE_URL}/Other/2.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Other,
    src: `${STORAGE_URL}/Other/3.png`
  },
  {
    type: 'image',
    kind: NekoType.Other,
    src: `${STORAGE_URL}/Other/4.png`
  },
  {
    type: 'image',
    kind: NekoType.Other,
    src: `${STORAGE_URL}/Other/5.gif`
  },
  {
    type: 'image',
    kind: NekoType.Other,
    src: `${STORAGE_URL}/Other/6.gif`
  },
  {
    type: 'image',
    kind: NekoType.Other,
    src: `${STORAGE_URL}/Other/7.png`
  },
  {
    type: 'image',
    kind: NekoType.Ragdoll,
    src: `${STORAGE_URL}/Ragdoll/1.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Ragdoll,
    src: `${STORAGE_URL}/Ragdoll/2.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Ragdoll,
    src: `${STORAGE_URL}/Ragdoll/3.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Ragdoll,
    src: `${STORAGE_URL}/Ragdoll/4.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Ragdoll,
    src: `${STORAGE_URL}/Ragdoll/5.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Ragdoll,
    src: `${STORAGE_URL}/Ragdoll/6.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Ragdoll,
    src: `${STORAGE_URL}/Ragdoll/7.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Ragdoll,
    src: `${STORAGE_URL}/Ragdoll/8.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Ragdoll,
    src: `${STORAGE_URL}/Ragdoll/9.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Ragdoll,
    src: `${STORAGE_URL}/Ragdoll/10.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Ragdoll,
    src: `${STORAGE_URL}/Ragdoll/11.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Ragdoll,
    src: `${STORAGE_URL}/Ragdoll/12.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Ragdoll,
    src: `${STORAGE_URL}/Ragdoll/13.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Ragdoll,
    src: `${STORAGE_URL}/Ragdoll/14.jpg`
  },
  {
    type: 'image',
    kind: NekoType.RussianBlue,
    src: `${STORAGE_URL}/RussianBlue/1.jpg`
  },
  {
    type: 'image',
    kind: NekoType.RussianBlue,
    src: `${STORAGE_URL}/RussianBlue/2.jpg`
  },
  {
    type: 'image',
    kind: NekoType.RussianBlue,
    src: `${STORAGE_URL}/RussianBlue/3.jpg`
  },
  {
    type: 'image',
    kind: NekoType.RussianBlue,
    src: `${STORAGE_URL}/RussianBlue/4.jpg`
  },
  {
    type: 'image',
    kind: NekoType.RussianBlue,
    src: `${STORAGE_URL}/RussianBlue/5.jpg`
  },
  {
    type: 'image',
    kind: NekoType.RussianBlue,
    src: `${STORAGE_URL}/RussianBlue/6.jpg`
  },
  {
    type: 'image',
    kind: NekoType.RussianBlue,
    src: `${STORAGE_URL}/RussianBlue/7.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ScottishFold,
    src: `${STORAGE_URL}/ScottishFold/1.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ScottishFold,
    src: `${STORAGE_URL}/ScottishFold/2.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ScottishFold,
    src: `${STORAGE_URL}/ScottishFold/3.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ScottishFold,
    src: `${STORAGE_URL}/ScottishFold/4.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ScottishFold,
    src: `${STORAGE_URL}/ScottishFold/5.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ScottishFold,
    src: `${STORAGE_URL}/ScottishFold/6.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ScottishFold,
    src: `${STORAGE_URL}/ScottishFold/7.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ScottishFold,
    src: `${STORAGE_URL}/ScottishFold/8.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ScottishFold,
    src: `${STORAGE_URL}/ScottishFold/9.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ScottishFold,
    src: `${STORAGE_URL}/ScottishFold/10.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ScottishFold,
    src: `${STORAGE_URL}/ScottishFold/11.jpg`
  },
  {
    type: 'image',
    kind: NekoType.ScottishFold,
    src: `${STORAGE_URL}/ScottishFold/12.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Somali,
    src: `${STORAGE_URL}/Somali/1.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Somali,
    src: `${STORAGE_URL}/Somali/2.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Somali,
    src: `${STORAGE_URL}/Somali/3.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Somali,
    src: `${STORAGE_URL}/Somali/4.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Somali,
    src: `${STORAGE_URL}/Somali/5.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Somali,
    src: `${STORAGE_URL}/Somali/6.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Somali,
    src: `${STORAGE_URL}/Somali/7.jpg`
  },
  {
    type: 'image',
    kind: NekoType.Somali,
    src: `${STORAGE_URL}/Somali/8.jpg`
  }
];
