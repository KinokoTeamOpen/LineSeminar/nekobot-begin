import { NekoType } from './neko-type';

export class Nekonverter {
  static convertKindToString(kind: NekoType): string {
    let ret;
    switch (kind) {
      /**
       * マンチカン
       */
      case NekoType.Munchkin:
        ret = 'マンチカン';
        break;
      case NekoType.ScottishFold:
        ret = 'スコティッシュフォールド';
        break;
      case NekoType.Ragdoll:
        ret = 'ラグドール';
        break;
      case NekoType.MaineCoon:
        ret = 'メインクーン';
        break;
      case NekoType.RussianBlue:
        ret = 'ロシアンブルー';
        break;
      case NekoType.ExoticShorthair:
        ret = 'エキゾチックショートヘア';
        break;
      case NekoType.AmericanShorthair:
        ret = 'アメリカンショートヘア';
        break;
      case NekoType.NorwegianForestCat:
        ret = 'ノルウェージャンフォレストキャット';
        break;
      case NekoType.Somali:
        ret = 'ソマリ';
        break;
      case NekoType.Bengal:
        ret = 'ベンガル';
        break;
      case NekoType.Other:
        ret = 'その他';
        break;
      default:
        new Error(`Unexpected NekoType: ${kind}`);
    }
    return ret;
  }
}
